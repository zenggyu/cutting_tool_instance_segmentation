#!/bin/bash

# 请确保在执行以下指令时：
# - 你已安装了miniconda3及相关软件包（本项目的`env`文件第一部分内容提供了详尽信息）
# - 工作目录位于此项目的根目录。

~/miniconda3/lib/python3.6/site-packages/keras_maskrcnn/bin/evaluate.py \
csv ./annotation/annotation.csv ./annotation/class_mapping.csv \
./model/resnet50_csv_256.h5

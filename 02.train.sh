#!/bin/bash

# 请确保在执行以下指令时：
# - 你已安装了miniconda3及相关软件包（本项目的`env`文件第一部分内容提供了详尽信息）
# - 工作目录位于此项目的根目录。

maskrcnn-train --gpu 1 --epochs 256 --steps 4096 \
--weights ./model/resnet50_coco_v0.2.0.h5 \
--snapshot-path ./model \
csv ./annotation/annotation.csv ./annotation/class_mapping.csv

# 载入相关模块
import keras

from keras_maskrcnn import models
from keras_maskrcnn.utils.visualization import draw_mask
from keras_retinanet.utils.visualization import draw_box, draw_caption, draw_annotations
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
from keras_retinanet.utils.colors import label_color

import cv2
import numpy as np
import json
from PIL import Image, ImageDraw

def get_instance_masks(model, img, labels_to_names):
  img = np.asarray(img)
  img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
  width, height, _ = img.shape
  img = preprocess_image(img)
  img, scale = resize_image(img)
  outputs = model.predict_on_batch(np.expand_dims(img, axis = 0))

  boxes  = outputs[-4][0]
  scores = outputs[-3][0]
  labels = outputs[-2][0]
  masks  = outputs[-1][0]

  boxes /= scale
  
  instance_masks = {}
  
  for label, label_name in labels_to_names.items():
    instance_masks[label_name] = []
  
  for box, score, label, mask in zip(boxes, scores, labels, masks):
    if score < 0.5:
        break
    mask = mask[:, :, label]
    box = box.astype(np.int)
    mask = mask.astype(np.float32)
    mask = cv2.resize(mask, (box[2] - box[0], box[3] - box[1]))
    mask = (mask > 0.5).astype(np.uint8)
    mask_image = np.zeros((width, height), np.uint8)
    mask_image[box[1]:box[3], box[0]:box[2]] = mask
    
    instance_masks[labels_to_names[label]].append(mask_image)
    
  return instance_masks

def locate_centroid(mask):
  y, x = np.nonzero(mask)
  (x, y) = (x.reshape((-1, 1)), y.reshape((-1, 1)))
  centroid = tuple(np.mean(np.concatenate((x, y), axis = 1), axis = 0).astype(np.int))
  return centroid

def generate_axes(mask, centroid, axis_alphas, pi = 3.1415926535):
  h, w = mask.shape
  (x0, y0) = centroid
  axes = np.zeros((len(axis_alphas), h, w), dtype = np.uint8)
  for i in range(len(axis_alphas)):
    if np.isclose(axis_alphas[i], 0):
      end_points = ((0, y0), (w, y0))
    elif np.isclose(axis_alphas[i], pi / 2):
      end_points = ((x0, 0), (x0, h))
    else:
      end_points = ((0, y0 - np.tan(axis_alphas[i]) * x0), (w, y0 + np.tan(axis_alphas[i]) * (w - x0)))
    axis = Image.new("L", (w, h))
    ImageDraw.Draw(axis).line(end_points, fill = 1, width = 0)
    axis = np.asarray(axis)
    axes[i] = axis
  return axes

def find_long_axis(mask, axes):
  lengths = np.zeros(len(axes))
  intersection = mask * axes
  for i in range(len(axes)):
    if np.all(intersection[i] == 0):
      lengths[i] = 0
    else:
      coordinates = np.vstack(np.nonzero(intersection[i]))
      lengths[i] = np.sqrt(np.sum((np.max(coordinates, axis = 1) - np.min(coordinates, axis = 1)) ** 2))
  return np.argmax(lengths)

def distance_vector(points, alpha, centroid, pi = 3.1415926535):
  (x0, y0) = centroid
  if np.isclose(alpha, 0):
    a = 0
    b = -1
    c = y0
    on_greater_side = points[:, 1] > y0
    unit_vector = np.array([0, 1])
  elif np.isclose(alpha, pi / 2):
    a = 1
    b = 0
    c = -x0
    on_greater_side = points[:, 0] > x0
    unit_vector = np.array([-1, 0])
  else:
    a = np.tan(alpha)
    b = -1
    c = y0 - np.tan(alpha) * x0
    on_greater_side = np.sum(np.array([a, b]) * points, axis = 1) + c < 0
    unit_vector = np.array([-np.tan(alpha), 1]) / np.sqrt(1 + np.tan(alpha) ** 2)
    
  distance = np.abs(np.sum(np.array([a, b]) * points, axis = 1) + c) / np.sqrt(a ** 2 + b ** 2)
  
  dist_vec_g = np.max(distance[on_greater_side]) * unit_vector
  dist_vec_ng = -np.max(distance[~on_greater_side]) * unit_vector

  return dist_vec_g, dist_vec_ng

def rotate_crop(img, mask, centroid, long_axis_alpha, pi = 3.1415926535):
  img = np.array(img)
  img[mask != 1] = 0
  img = Image.fromarray(img)
  points = np.transpose(np.nonzero(mask))[:,::-1]
  short_axis_alpha = long_axis_alpha + pi / 2
  dist_vec_gl, dist_vec_ngl = distance_vector(points, long_axis_alpha, centroid)
  dist_vec_gs, dist_vec_ngs = distance_vector(points, short_axis_alpha, centroid)
  key_points = np.vstack((centroid, centroid + dist_vec_gl + dist_vec_gs, centroid + dist_vec_ngl + dist_vec_ngs)).astype(np.int)
  degree = (pi / 2 - long_axis_alpha) / pi * 180
  img = img.rotate(-degree, expand = True, resample = Image.BICUBIC)
  
  key_points = key_points - np.array(mask.shape[::-1]) / 2
  key_points = np.dot(np.array([[np.cos(pi / 2 - long_axis_alpha), -np.sin(pi / 2 - long_axis_alpha)],\
                                [np.sin(pi / 2 - long_axis_alpha), np.cos(pi / 2 - long_axis_alpha)]]),\
                      np.transpose(key_points)).T
  key_points = key_points + np.array(img.size) / 2
  centroid, diagonals = key_points[0], key_points[1:]
  diagonals = tuple(np.vstack((np.min(diagonals, axis = 0), np.max(diagonals, axis = 0))).ravel().astype(int))
  centroid_vec = centroid - np.array(img.size)
  cos, sin = tuple(centroid_vec / np.sqrt(np.sum(centroid_vec**2)))
  img = img.crop(diagonals)
  # 以实例的重心为原点划分四个象限，对图像进行翻转或旋转，使含有最多像素点的象限位于第一象限；此操作的意义在于进一步减少由拍摄角度不同而带来的变异
  if sin >= 0 and cos > 0:
    pass
  elif sin > 0 and cos <= 0:
    img = img.transpose(Image.FLIP_LEFT_RIGHT)
  elif sin <= 0 and cos < 0:
    img = img.rotate(180)
  elif sin < 0 and cos >= 0:
    img = img.transpose(Image.FLIP_TOP_BOTTOM)
  return img

def get_instance_crops(img, masks, pi = 3.1415926535):
  crops = {}
  for label_name in masks.keys():
    crops[label_name] = []
    for i in range(len(masks[label_name])):
      mask = masks[label_name][i]
      assert(isinstance(img, Image.Image) and isinstance(mask, np.ndarray))
      assert(img.size == tuple(reversed(mask.shape)))
      assert(np.any(mask != 0))
      centroid = locate_centroid(mask)
      n_axes = 12
      axis_alphas = [i / n_axes * pi for i in range(n_axes)]
      axes = generate_axes(mask, centroid, axis_alphas)
      long_axis_alpha = axis_alphas[find_long_axis(mask, axes)]
      crops[label_name].append(rotate_crop(img, mask, centroid, long_axis_alpha))
  return crops

def load_labels(file_path):
  with open(file_path) as file:
    labels_to_names = json.loads(file.readlines()[0])["region"]["class_number"]["options"]
    labels = list(labels_to_names.keys())
    for label in labels:
      labels_to_names[int(label)] = labels_to_names.pop(label)
  return labels_to_names

def visualize(image, labels_to_names):
  image = np.array(image)
  draw = image.copy()
  image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

  image = preprocess_image(image)
  image, scale = resize_image(image)

  outputs = model.predict_on_batch(np.expand_dims(image, axis=0))

  boxes  = outputs[-4][0]
  scores = outputs[-3][0]
  labels = outputs[-2][0]
  masks  = outputs[-1][0]

  boxes /= scale

  for box, score, label, mask in zip(boxes, scores, labels, masks):
      if score < 0.5:
          break

      color = label_color(label)

      b = box.astype(int)
      draw_box(draw, b, color=color)

      mask = mask[:, :, label]
      draw_mask(draw, b, mask, color=label_color(label))

      caption = "{} {:.2f}".format(labels_to_names[label], score)
      draw_caption(draw, b, caption)
  return Image.fromarray(draw)

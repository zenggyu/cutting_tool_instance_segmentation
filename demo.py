# 载入相关程序和模块
exec(open('04.postprocess.py').read())

# 载入模型
model = models.load_model('model/resnet50_csv_256.h5', backbone_name='resnet50')

# 载入标注配置文件（该文件可以从<https://gitlab.com/zenggyu/cutting_tool_image_annotation/>获取）
labels_to_names = load_labels("../cutting_tool_image_annotation/attributes.json")

# 载入需要进行实例分割的jpg图像
img = Image.open('path_to_image.jpg')

# 对实例分割结果进行可视化
visualize(img, labels_to_names)

# 截取实例分割结果（`crops`中含有各个分类实例的截取结果）
masks = get_instance_masks(model, img, labels_to_names)
crops = get_instance_crops(img, masks)

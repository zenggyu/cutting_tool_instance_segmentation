# 刀具实例分割项目简介

[基于内容的图像检索](https://zh.wikipedia.org/wiki/%E5%9F%BA%E4%BA%8E%E5%86%85%E5%AE%B9%E7%9A%84%E5%9B%BE%E5%83%8F%E6%A3%80%E7%B4%A2)技术能被用于检索与给定商品外观相似的商品，进而实现专利外观侵权检测的目的。该技术的核心在于通过提取图像特征并计算特征间的距离，以决定两个图像的相似程度。尽管目标商品的图像特征应该是决定商品图像是否相似的主要因素，但在实际应用中，图像相似度及检索结果还可能受到其他因素的影响，例如：

- 图像中所包含的背景、杂物；
- 图像是否包含多个不同的商品实例；
- 商品的拍摄角度。

上述因素均可能使检索质量下降，并且在训练数据少时影响尤为明显。解决这些问题的一种有效方式是对图像中的商品进行[实例分割](https://zh.wikipedia.org/wiki/%E5%9B%BE%E5%83%8F%E5%88%86%E5%89%B2)以去除背景杂物，并通过对齐分割结果以减少拍摄角度的影响。

本项目是某刀具外观侵权检测项目的前期铺垫，其主要目标就是通过上述方式识别、提取电商图片中所含的刀具实例，以便用于后续的相似度计算；具体而言，本项目将编写程序实现以下主要任务：

- 对刀具图像数据（[数据来源](https://gitlab.com/zenggyu/cutting_tool_image_annotation/)）进行预处理，使之适应实例分割模型训练的需要（详见`01.preprocess.R`）；
- 使用由FIZYR团队开发的Mask R-CNN算法实现（[项目地址](https://github.com/fizyr/keras-maskrcnn)）训练实例分割模型，并评估模型性能（详见`02.train.sh`及`03.evaluate.sh`）；
- 提取分割结果并使之尽量对齐，以便用于计算商品相似度（详见`04.postprocess.py`）。

训练好的模型可以从[此处下载](https://gitlab.com/zenggyu/cutting_tool_instance_segmentation/raw/3767271d1e935b228962a662d54e017dcddfc22d/model/resnet50_csv_256.h5)；`demo.py`文件提供了模型的使用方法，而有关程序的版本信息请参阅`env`文件。以下将对上述任务的主要内容进行介绍。

# 图像预处理

此过程的主要工作包括提取图像及图像标注数据，并将其转换成符合模型训练要求的表达形式；除此以外，还包括数据扩增（data augmentation），即对训练集中的每幅图像添加一些随机或非随机的变换，得到多张相似而又不同的图像以增加训练集的大小，进而避免模型的过拟合。本项目使用以下图像操作的随机组合，按1:8的比例对图像进行扩增：

- 旋转
- 翻转
- 高斯模糊/动作模糊
- 高斯噪点
- 对比度变化
- 灰度/色调/饱和度变化
- 像素增强
- 颜色通道交换
- Jpeg格式压缩损耗

以下是数据扩增示例：

![数据扩增](doc/fig/fig01.png)

# 模型训练与评估

在完成图像预处理工作后，执行本项目`02.train.sh`中的指令，即可开始训练。

目前，该模型可以识别5类刀具，包括：`knife`（如菜刀、水果刀、军刀等）、`scissors`（剪刀）、`sword`（剑）、`nailclipper`（指甲刀）、`peeler`（削皮刀）。下图显示了该模型的输出结果，包括各刀具实例的分类及概率（见图中文字）、定位框（见图中矩形框），以及mask（见图中半透明色块）：

![实例分割](doc/fig/fig02.png)

通过运行本项目`02.evaluate.sh`中的指令，可以对模型的性能进行评估；以下是模型在训练集上的评估结果（有关指标的解释可以[看这里](https://blog.zenggyu.com/en/post/2018-12-16/an-introduction-to-evaluation-metrics-for-object-detection/)）：

```
6207 instances of class knife with average precision: 0.6811
1608 instances of class scissors with average precision: 0.9863
376 instances of class sword with average precision: 0.6170
216 instances of class nailclipper with average precision: 1.0000
264 instances of class peeler with average precision: 1.0000
mAP: 0.8569
```

# 图像后处理

本项目在获得实例分割模型后，对每幅图像进行以下操作，以获得其中所包含的刀具实例图像（对于含有多个刀具实例的图像，以下过程中的第1步将得到多个分割结果，此时第2-6步应分别针对每个分割结果分别进行）：

1. 对原始图像进行实例分割，获得刀具实例的mask（该结果包含一个刀具实例在图像中所覆盖的像素点位置的描述）；
2. 根据mask计算实例的重心坐标；
3. 以实例重心为原点，定义12条斜率不同的轴线；
4. 计算各轴线与实例mask相交的长度，并取距离最长者作为长轴；
5. 对图像进行旋转，使实例的长轴位于垂直方向；
6. 根据旋转后的图像及mask，截取刀具实例图像，并将实例范围之外的像素点以黑色填充。

经过上述过程，即可去除图像中的背景杂物，并使所得到的刀具长轴均位于垂直方向，减少拍摄角度带来的变异（见下图）。

![原始图像与mask](doc/fig/fig03.png)

![图像后处理](doc/fig/fig04.png)

# 结果展示

![](doc/fig/result01.png)

![](doc/fig/result02.png)

![](doc/fig/result03.png)

![](doc/fig/result04.png)

![](doc/fig/result05.png)

![](doc/fig/result06.png)

![](doc/fig/result07.png)

![](doc/fig/result08.png)

![](doc/fig/result09.png)

![](doc/fig/result10.png)

![](doc/fig/result11.png)

![](doc/fig/result12.png)

# 参考文献

[1] Fu, Cheng-Yang, Mykhailo Shvets, and Alexander C. Berg. 2019. “RetinaMask: Learning to Predict Masks Improves State-of-the-Art Single-Shot Detection for Free,” January. http://arxiv.org/abs/1901.03353.

[2] He, Kaiming, Georgia Gkioxari, Piotr Dollar, and Ross Girshick. 2017. “Mask R-CNN.” In 2017 IEEE International Conference on Computer Vision (ICCV), 2980–8. Venice: IEEE. doi:10.1109/ICCV.2017.322.

[3] Zeng, Guangyu. 2019. Beyond RetinaNet and Mask R-CNN: Single-shot Instance Segmentation with RetinaMask. https://blog.zenggyu.com/en/post/2019-01-07/beyond-retinanet-and-mask-r-cnn-single-shot-instance-segmentation-with-retinamask/

[4] Zeng, Guangyu. 2018. RetinaNet Explained and Demystified. https://blog.zenggyu.com/en/post/2018-12-05/retinanet-explained-and-demystified/

[5] Gaiser, Hans, et al. 2018. Fizyr/Keras-Maskrcnn: 0.2.0. Zenodo. DataCite, doi:10.5281/zenodo.1464000. https://github.com/fizyr/keras-maskrcnn/
